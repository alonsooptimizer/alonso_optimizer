/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of AlonsoOptim.
  
  AlonsoOptim is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  RealTimePlot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with AlonsoOptim. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include "alonso_optim/alonso_optim.h"
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <cmath>

#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

//Number of data points
int data_dim = 100;

//Will contain the data to fit
std::vector<double> xs, ys;

using namespace alonso_optim;

//Define fitness function
double rsquare( Parameters pars ) {
    double sum = 0;
    for (int i=0; i<xs.size(); ++i) {
        double exp_y = pars["a"]*xs[i]+pars["b"];
        sum += pow(exp_y-ys[i],2);
    }
    return 1.0/sum;
}

int main() {
    //Random number generator
    const gsl_rng_type * T;
    gsl_rng * generator;
    gsl_rng_env_setup();

    T = gsl_rng_default;
    generator = gsl_rng_alloc (T);

    //Create fake data, use linear relation ship with noise around it
    //a*x+b (a and b (and sigma?) are the parameters that will need fitting)
    double a = 1.0;
    double b = 10;
    for (int i=0; i<data_dim; ++i) {
        double x = i; //or random uniform draw between 0 and 100?
        double y = a*x+b+gsl_ran_gaussian( generator, 5.0 ); //+noise
        xs.push_back( x );
        ys.push_back( y );
    }

    //Define parameters (ranges etc) to optimize
    Parameters fitting_parameters;
    fitting_parameters.push_back( Parameter( "a", 0, 10, 0.1 ) );
    fitting_parameters.push_back( Parameter( "b", 0, 50, 0.1 ) );

    fitting_parameters.randomize( generator );
    std::cout << rsquare( fitting_parameters ) << std::endl;

    std::vector<int> step_changes;
    std::vector<std::string> step_types;

    step_changes.push_back( 100 );
    step_changes.push_back( 200 );
    step_changes.push_back( 500 );
    step_types.push_back( "asexual" );
    step_types.push_back( "sexual" );
    step_types.push_back( "hill" );
    step_types.push_back( "asexual" );

    //Run optimizer
    Optimizer optim = Optimizer( rsquare, fitting_parameters, step_changes,
            step_types, generator );
    for (int i=0; i<1000; ++i) {
        std::cout << "Fitnes: " << optim.best_fit.fitness << std::endl;
        std::cout << optim.best_fit.parameters["a"] << std::endl;
        std::cout << optim.best_fit.parameters["b"] << std::endl;
        optim.step();
    }
    optim.print();
    return 0;
}
