/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of AlonsoOptim.
  
  AlonsoOptim is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  RealTimePlot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with AlonsoOptim. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#ifndef ALONSO_OPTIM_H
#define ALONSO_OPTIM_H

#include <vector>
#include <map>
#include <string>
#include <gsl/gsl_rng.h>

namespace alonso_optim {
    /**
     * \brief Parameter class used to set max and min etc of the search range
     */
    class Parameter {
        public:
            double min, max, mutation_sigma, value;
            std::string name;

            /**
             * \brief Dummy constructor
             */
            Parameter() {};

            Parameter( std::string name, double min, double max, 
                    double mutation_sigma );
            Parameter( const Parameter& copy );

    };

    /**
     * \brief Class to keep track and access all variables
     */
    class Parameters {
        public:
            Parameters() {};
            Parameters( const Parameters& copy );

            void push_back( Parameter par );
            double& operator[]( std::string key ); 

            Parameter& random_parameter( gsl_rng * generator );
            
            Parameters crossover( Parameters pars2, gsl_rng * generator );

            void randomize( gsl_rng * generator );

            void print();

        private:
            std::map<std::string, Parameter> pars;
            std::vector<std::string> labels;
    };

    /**
     * \brief Class to keep track of "gen" with the parameter values
     *
     * Also takes care of reproduction/mutation
     */
    class Individual {
        public:
            double fitness;
            gsl_rng * generator;

            double (*fitness_function)( Parameters );
            Parameters parameters;

            /**
             * \brief Dummy constructor
             */
            Individual() {};

            Individual( double (*fit_function)( Parameters ), 
                    Parameters pars, gsl_rng * generator );

            /**
             * \brief Mutate an allele
             */
            void mutate();

            /**
             * \brief Sexual procreation returns a new individual
             */
            Individual sexual_procreation( Individual &partner );

            /**
             * \brief Asexual procreation returns a new individual
             */
            Individual asexual_procreation();

            /**
             * \brief Do a step up the hill
             *
             * Calls mutation to determine size of the step and then checks 
             * fitness of the new gene. If the new fitness is lower then ignore
             * the mutation.
             */
            void step_up_the_hill();
            
            /**
             * \brief Allows one to compare two individuals based on fitness
             *
             * Usefull if one wants to sort individuals according to fitness
             */
            bool operator<( const Individual& other );
    };

    /**
     * \brief Class that controls the genetic algorithm
     */
    class GeneticAlgorithm {
        public:
            std::vector<Individual*> Population;
    };

    /**
     * \brief Class that controls the Optimizer 
     */
    class Optimizer {
        public:
            gsl_rng * generator;

            Individual best_fit;

            int step_count, step_id;
            
            std::vector<int> step_changes;
            std::vector<std::string> step_types;

            Parameters parameter_boundaries;
            std::vector<Individual> population;
            int population_size;
            double (*fitness_function)( Parameters );

            Optimizer();

            /**
             * \brief Initializer of Optimizer
             *
             * \param fit_function a function that takes Parameters and 
             * returns the fitness (positive value) of those parameters
             * \param changes a vector that specifies at which step to change 
             * the fitting method
             * \param types a vector that specifies which fitting method to change to. 
             * Valid values are "sexual", "asexual" and "hill"
             */
            Optimizer( double (*fit_function)( Parameters ), 
                    Parameters param_boundaries, 
                    std::vector<int> changes, std::vector<std::string> types,
                    gsl_rng * generator );
            
            /**
             * \brief initialize a random population
             */
            void initialize_population();

            /**
             * \brief Choose a random individu
             */
            Individual& random_individual();

            /**
             * \brief Choose a random individual, probability of being choses is dependent on fitness of the individual
             */
            Individual& random_individual_based_on_fitness( double total_fitness );

            /**
             * \brief Create the next generation, using asexual reproduction
             */
            void next_generation_asexual();

            /**
             * \brief Create the next generation, using asexual reproduction
             */
            void next_generation_sexual();

            /**
             * \brief Perform one fitting step
             */
            void step();

            /**
             * \brief Print current best parameters
             */
            void print();
    };

}
#endif
