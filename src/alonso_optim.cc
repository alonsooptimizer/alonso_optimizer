/*
  -------------------------------------------------------------------
  
  Copyright (C) 2010, Edwin van Leeuwen
  
  This file is part of AlonsoOptim.
  
  AlonsoOptim is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.
  
  RealTimePlot is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with AlonsoOptim. If not, see <http://www.gnu.org/licenses/>.

  -------------------------------------------------------------------
*/

#include <iostream>

#include "alonso_optim/alonso_optim.h"
#include <gsl/gsl_randist.h>
#include <cmath>

namespace alonso_optim {
    Parameter::Parameter( std::string new_name, double new_min, double new_max, 
                    double new_mutation_sigma ) {
        name = new_name;
        min = new_min;
        max = new_max;
        mutation_sigma = new_mutation_sigma;
    }

    Parameter::Parameter( const Parameter& copy ) {
        name = copy.name;
        min = copy.min;
        max = copy.max;
        mutation_sigma = copy.mutation_sigma;
        value = copy.value;
     }

    /*
     * Parameters
     */
    Parameters::Parameters( const Parameters& copy ) {
        std::map<std::string, Parameter>::const_iterator itr;

        for(itr = copy.pars.begin(); itr != copy.pars.end(); ++itr){
            push_back( Parameter( (*itr).second ) );
        }
    }

    
    void Parameters::push_back( Parameter par ) {
        labels.push_back( par.name );
        pars[par.name] = par;
     }

    double& Parameters::operator[]( std::string key ) {
        return pars[key].value;
    }

    Parameter& Parameters::random_parameter( gsl_rng * generator ) {
        int id = floor( gsl_ran_flat( generator, 0, labels.size() ) );
        //if the random call was exactly labels.size then that would result in an error
        //so make sure that didn't happen 
        if (id == labels.size())
            --id;
        return pars[labels[id]];
    }

    Parameters Parameters::crossover( Parameters pars2, gsl_rng * generator ) {
        Parameters new_pars( *this );
        int id = floor( gsl_ran_flat( generator, 0, labels.size()+1 ) );
        double start_at = gsl_ran_flat( generator, 0, 1 );
        Parameters start_pars;
        Parameters end_pars;

        if (start_at<0.5) {
            start_pars = Parameters( *this );
            end_pars = pars2;
        } else {
            start_pars = pars2;
            end_pars = Parameters( *this );
        }

        /*print();
        std::cout << "start_pars" << std::endl;
        start_pars.print();
        pars2.print();
        std::cout << "end_pars" << std::endl;
        end_pars.print();*/


        for(int i=0; i<labels.size(); ++i) {
            if (i<id) {
                new_pars[labels[i]] = start_pars[labels[i]];
            } else {
                new_pars[labels[i]] = end_pars[labels[i]];
            }
        }

        /*new_pars.print();*/
 
        return new_pars;
    }

    void Parameters::randomize( gsl_rng * generator ) {
        std::map<std::string, Parameter>::iterator itr;

        for(itr = pars.begin(); itr != pars.end(); ++itr) {
            double rnd_value = gsl_ran_flat( generator, 
                    (*itr).second.min, (*itr).second.max );
            (*itr).second.value = rnd_value;
        }
    }

    void Parameters::print() {
        std::map<std::string, Parameter>::iterator itr;

        for(itr = pars.begin(); itr != pars.end(); ++itr) {
            std::cout << (*itr).first << ": " << (*itr).second.value << std::endl;
        }
     }

    Individual::Individual( double (*fit_function)( Parameters ), 
            Parameters pars, gsl_rng * r ) { 
        fitness_function = fit_function;
        parameters = pars;
        generator = r;
        fitness = fitness_function( parameters );
    }

    void Individual::mutate() {
        Parameter& par = parameters.random_parameter( generator );
        double new_value = par.value + gsl_ran_gaussian( generator, 
                par.mutation_sigma );
        if (new_value > par.max)
            par.value = par.max;
        else if (new_value < par.min)
            par.value = par.min;
        else
            par.value = new_value;
        fitness = fitness_function( parameters );
    }

    Individual Individual::sexual_procreation( Individual &partner ) {
        //for now just one mutation
        Individual ind1 = Individual( fitness_function, parameters, generator );
        Individual ind2 = Individual( partner.fitness_function, partner.parameters, 
                partner.generator );
        ind1.mutate();
        ind2.mutate();
        ind1.parameters = ind1.parameters.crossover( ind2.parameters, generator );
        ind1.fitness = ind1.fitness_function( ind1.parameters );
        /*std::cout << ind1.fitness << std::endl;
        ind1.parameters.print();*/
        return ind1;
    }

    Individual Individual::asexual_procreation() {
        //for now just one mutation
        Individual ind = Individual( fitness_function, parameters, generator );
        ind.mutate();
        return ind;
    }

    void Individual::step_up_the_hill() {
        Individual ind = Individual( fitness_function, parameters, generator );
        ind.mutate();
        if ( ind.fitness > fitness ) {
            parameters = ind.parameters;
            fitness = ind.fitness;
        }
    }

    bool Individual::operator<( const Individual& comp ) {
        return fitness<comp.fitness;
    }

    Optimizer::Optimizer() {}

    Optimizer::Optimizer( double (*fit_function)( Parameters ), 
            Parameters param_boundaries, 
            std::vector<int> changes, std::vector<std::string> types,
            gsl_rng * r ) {
        parameter_boundaries = param_boundaries;
        generator = r;
        population_size = 100;
        fitness_function = fit_function;

        step_changes = changes;
        step_types = types;
        step_id = 0;
        step_count = 0;

        initialize_population();
    }

    void Optimizer::initialize_population() {
        for (int i=0; i<population_size; ++i) {
            Parameters new_params = Parameters( parameter_boundaries );
            new_params.randomize( generator );
            Individual ind = Individual( fitness_function, new_params, generator );
            population.push_back( ind );
            ind.fitness = fitness_function( ind.parameters );
            if (i == 0) 
                best_fit = ind;
            if (ind.fitness > best_fit.fitness)
                best_fit = ind;
        }
    }

    Individual& Optimizer::random_individual() {
        int id = floor( gsl_ran_flat( generator, 0, population.size() ) );
        //if the random call was exactly labels.size then that would result in an error
        //so make sure that didn't happen 
        if (id == population.size())
            --id;
        return population[id];
    }

    Individual& Optimizer::random_individual_based_on_fitness( 
            double total_fitness ) {
        double rnd = gsl_ran_flat( generator, 0, total_fitness );
        double sum_fitness = 0;

        std::vector<Individual>::iterator itr;

        for(itr = population.begin(); itr != population.end(); ++itr){
            sum_fitness += (*itr).fitness;
            if (sum_fitness>=rnd) 
                return (*itr);
        }

        std::cout << "Shouldn't happen!" << std::endl;
        return population.back();
     }

    void Optimizer::next_generation_sexual() {

        //calculate total_fitness
        double total_fitness = 0;
        std::vector<Individual>::iterator itr;

        for(itr = population.begin(); itr != population.end(); ++itr){
            total_fitness += (*itr).fitness;
        }

        //build new population
        std::vector<Individual> new_population;
        for (int i=0; i<population.size(); ++i) {
            Individual& ind1 = random_individual_based_on_fitness( total_fitness );
            Individual& ind2 = random_individual_based_on_fitness( total_fitness );
            Individual ind( ind1 );
            if (gsl_ran_flat( generator, 0, 1.0 )>0.5)
                ind = ind1.sexual_procreation( ind2 );
                new_population.push_back( ind );
            if (ind.fitness>best_fit.fitness)
                best_fit = ind;
        }
        population = new_population;
    }

    void Optimizer::next_generation_asexual() {

        //calculate total_fitness
        double total_fitness = 0;
        std::vector<Individual>::iterator itr;

        for(itr = population.begin(); itr != population.end(); ++itr){
            total_fitness += (*itr).fitness;
        }

        //build new population
        std::vector<Individual> new_population;
        for (int i=0; i<population.size(); ++i) {
            Individual ind = random_individual_based_on_fitness( total_fitness );
            if (gsl_ran_flat( generator, 0, 1.0 )>0.5)
                ind = ind.asexual_procreation();
            new_population.push_back( ind );
            if (ind.fitness>best_fit.fitness)
                best_fit = ind;
        }
        population = new_population;
    }

    void Optimizer::step() {
        /* Walk up the hill
        Individual& ind = random_individual();
        ind.step_up_the_hill();
        if (ind.fitness > best_fit.fitness) 
        best_fit = ind;*/
        if (step_count == step_changes[step_id])
            ++step_id;

        if (step_types[step_id] == "asexual" ) {
            next_generation_asexual();
        } else if (step_types[step_id] == "sexual" ) {
            next_generation_sexual();
        } else if (step_types[step_id] == "hill" ) {
            for (int i = 0; i<population.size(); ++i) {
                Individual& ind = random_individual();
                ind.step_up_the_hill();
                if (ind.fitness > best_fit.fitness) 
                    best_fit = ind;
            }
        } else {
            std::cout << "No fitting method specified" << std::endl;
        }
        ++step_count;

    }
    
    void Optimizer::print() {
        std::cout << "Fitness: " << best_fit.fitness << std::endl;
        best_fit.parameters.print();
    }

}
